package com.myspace.testproj;

import java.util.HashMap;
import java.util.Map;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.task.TaskEvent;
import org.kie.api.task.TaskLifeCycleEventListener;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.kie.internal.runtime.manager.RuntimeManagerRegistry;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
import org.kie.api.runtime.process.ProcessInstance; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import com.thoughtworks.xstream.core.util.Base64Encoder;
import org.json.simple.JSONObject;

public class TaskEventListener implements TaskLifeCycleEventListener {
	private RuntimeManagerRegistry registry = RuntimeManagerRegistry.get();
	
	String authStringEnc = baseAuthString();

	private static final Logger logger = LoggerFactory
			.getLogger(TaskEventListener.class);

	public TaskEventListener() {
	}

	public void afterTaskActivatedEvent(Task ti) {
		System.out.println("afterTaskActivatedEvent called....");
	}

	public void afterTaskClaimedEvent(Task ti) {
		System.out.println("afterTaskClaimedEvent called....");
	}

	public void afterTaskSkippedEvent(TaskEvent event) {
		System.out.println("afterTaskSkippedEvent called....");
	}

	public void afterTaskStartedEvent(Task ti) {
		System.out.println("afterTaskStartedEvent called....");
	}

	public void afterTaskStoppedEvent(Task ti) {
		System.out.println("afterTaskStoppedEvent called....");
	}

	public void afterTaskCompletedEvent(TaskEvent event) {
		System.out.println("afterTaskCompletedEvent called....");
	}

	public void afterTaskFailedEvent(TaskEvent event) {
		System.out.println("afterTaskFailedEvent called....");
	}

	public void afterTaskAddedEvent(TaskEvent event) {
		System.out.println("afterTaskAddedEvent called....");
		
		
		try{
		    Client client = Client.create();
		    AsyncWebResource webResource = client.asyncResource("http://localhost:8161/api/message/myQueue?type=queue");
		    //Future<ClientResponse> response = webResource.type("application/json").header("Authorization", "Basic " + authStringEnc).post(ClientResponse.class,jsonObject.toString());
		    JSONObject jsonObject = new JSONObject();
            RuntimeManager manager = getManager(event.getTask());
            Long processInstanceId = event.getTask().getTaskData().getProcessInstanceId();
            RuntimeEngine runtime = manager.getRuntimeEngine(ProcessInstanceIdContext.get(processInstanceId));
            KieSession kieSession = runtime.getKieSession();
            
            ProcessInstance pi = kieSession.getProcessInstance(processInstanceId);	

            jsonObject.put("containerId",event.getTask().getTaskData().getDeploymentId());
            jsonObject.put("processId",pi.getProcessId());
		    jsonObject.put("taskname",event.getTask().getName());
		    jsonObject.put("processInstanceId", processInstanceId);
	        jsonObject.put("taskId", event.getTask().getId());
	        jsonObject.put("assignee",event.getTask().getTaskData().getActualOwner().getId());
	        System.out.println("jsonObject : "+jsonObject.toString());       
	        
	        String message = event.getTask().getName()+"#"+event.getTask().getTaskData().getProcessInstanceId()+"#"+event.getTask().getId()+"#"+event.getTask().getTaskData().getActualOwner().getId();
	        System.out.println("message : "+message);
		    Future<ClientResponse> response = webResource.type("application/json").header("Authorization", "Basic " + authStringEnc).post(ClientResponse.class, jsonObject.toString());
		    System.out.println("response : "+response.get().getStatus());
		    //com.myspace.testproj.JMSUtil.sendMessage("Test message 345");
		    System.out.println("Message sent");
		}
		/*catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		catch(Exception ex){
		    ex.printStackTrace();
		}
		System.out.println("jms message sent called....");
		
	}

	public void afterTaskExitedEvent(TaskEvent event) {
		//JMSUtil.sendMessage("Okay");
		System.out.println("afterTaskAddedEvent called....");
		//com.ApacheMQUtil.sendMessage("test jms message");
	}

	@Override
	public void afterTaskReleasedEvent(TaskEvent event) {
		System.out.println("afterTaskReleasedEvent called....");
	}

	@Override
	public void afterTaskResumedEvent(TaskEvent event) {
		System.out.println("afterTaskResumedEvent called....");
	}

	@Override
	public void afterTaskSuspendedEvent(TaskEvent event) {
		System.out.println("afterTaskSuspendedEvent called....");
	}

	@Override
	public void afterTaskForwardedEvent(TaskEvent event) {
		System.out.println("afterTaskForwardedEvent called....");
	}

	@Override
	public void afterTaskDelegatedEvent(TaskEvent event) {
		System.out.println("afterTaskDelegatedEvent called....");
	}

	@Override
	public void beforeTaskActivatedEvent(TaskEvent event) {
		System.out.println("beforeTaskActivatedEvent called....");
		//com.ApacheMQUtil.sendMessage("test jms message");
	}

	@Override
	public void beforeTaskClaimedEvent(TaskEvent event) {
		System.out.println("beforeTaskClaimedEvent called....");
	}

	@Override
	public void beforeTaskSkippedEvent(TaskEvent event) {
		System.out.println("beforeTaskSkippedEvent called....");
	}

	@Override
	public void beforeTaskStartedEvent(TaskEvent event) {
		System.out.println("beforeTaskStartedEvent called....");
	}

	@Override
	public void beforeTaskStoppedEvent(TaskEvent event) {
		System.out.println("beforeTaskStoppedEvent called....");
	}

	@Override
	public void beforeTaskCompletedEvent(TaskEvent event) {
		System.out.println("beforeTaskCompletedEvent called....");
	}

	@Override
	public void beforeTaskFailedEvent(TaskEvent event) {
		System.out.println("beforeTaskFailedEvent called....");
	}

	@Override
	public void beforeTaskAddedEvent(TaskEvent event) {
		System.out.println("beforeTaskAddedEvent called....");
	}

	@Override
	public void beforeTaskExitedEvent(TaskEvent event) {
		System.out.println("beforeTaskExitedEvent called....");
	}

	@Override
	public void beforeTaskReleasedEvent(TaskEvent event) {
		System.out.println("beforeTaskReleasedEvent called....");
	}

	@Override
	public void beforeTaskResumedEvent(TaskEvent event) {
		System.out.println("beforeTaskResumedEvent called....");

	}

	@Override
	public void beforeTaskSuspendedEvent(TaskEvent event) {
		System.out.println("beforeTaskSuspendedEvent called....");
	}

	@Override
	public void beforeTaskForwardedEvent(TaskEvent event) {
		System.out.println("beforeTaskForwardedEvent called....");
	}

	@Override
	public void beforeTaskDelegatedEvent(TaskEvent event) {
		System.out.println("beforeTaskDelegatedEvent called....");
	}

	@Override
	public void afterTaskActivatedEvent(TaskEvent event) {
		System.out.println("afterTaskActivatedEvent called....");
	}

	@Override
	public void afterTaskClaimedEvent(TaskEvent event) {
		System.out.println("afterTaskClaimedEvent called....");
	}

	@Override
	public void afterTaskStartedEvent(TaskEvent event) {
		System.out.println("afterTaskStartedEvent called....");
	}

	@Override
	public void afterTaskStoppedEvent(TaskEvent event) {
		System.out.println("afterTaskStoppedEvent called....");
	}

	@Override
	public void beforeTaskNominatedEvent(TaskEvent event) {
		System.out.println("beforeTaskNominatedEvent called....");
	}

	@Override
	public void afterTaskNominatedEvent(TaskEvent event) {
		System.out.println("afterTaskNominatedEvent called....");
	}
	
	public String baseAuthString() {
        String username ="admin";
        String password="admin";
        String authString = username + ":" + password;
        String authStringEnc = new Base64Encoder().encode(authString.getBytes());
        return authStringEnc;
    }
    
    public RuntimeManager getManager(Task task) {           
        return registry.getManager(task.getTaskData().getDeploymentId());
        
    }
	
}